#ifndef GETSET_H
#define GETSET_H

template <typename T>
class GetSet
{
public:
	using set = void(*)(T);
	using get = T(*)();

	//General constructor
	GetSet() {}
	GetSet(get pGet, set pSet) : mGet(pGet), mSet(pSet) {}

	//Operators that allow easy interface
	void operator=(T const & pInput)
	{
		mSet(pInput);
	}
	T operator()()
	{
		return mGet();
	}
	operator T()
	{
		return mGet();
	}
private:
	get mGet;
	set mSet;
};

#endif