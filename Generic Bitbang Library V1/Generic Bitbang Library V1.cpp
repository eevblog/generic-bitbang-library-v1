// Generic Bitbang Library V1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "SPI.h"
#include "I2C.h"

using namespace std;

/////////////////////////////////////////////////////
Pin::Logic MISO, MOSI, CS, CLK;
std::string MISO_STR = "", MOSI_STR = "", CS_STR = "", CLK_STR = "";
void SPI_Record()
{
	MISO_STR	+= ',';
	MOSI_STR	+= ',';
	CLK_STR		+= ',';
	CS_STR		+= ',';
	MISO_STR	+= MISO + '0';
	MOSI_STR	+= MOSI + '0';
	CLK_STR		+= CLK + '0';
	CS_STR		+= CS + '0';
}
void SPI_ResetRecord()
{
	MISO_STR = "";
	MOSI_STR = "";
	CLK_STR = "";
	CS_STR = "";
}
void SPI_Print()
{
	cout << MISO_STR.substr(1) << endl;
	cout << MOSI_STR.substr(1) << endl;
	cout << CLK_STR.substr(1) << endl;
	cout << CS_STR.substr(1) << endl;
}

void SetMISO(Pin::Logic pInput)
{
	SPI_Record();
	MISO = pInput;
	SPI_Record();
}
void SetMOSI(Pin::Logic pInput)
{
	SPI_Record();
	MOSI = pInput;
	SPI_Record();
}
void SetCS	(Pin::Logic pInput)
{
	SPI_Record();
	CS = pInput;
	SPI_Record();
}
void SetCLK	(Pin::Logic pInput)
{
	SPI_Record();
	CLK = pInput;
	SPI_Record();
}

Pin::Logic GetMISO()
{	
	return MISO;
}	 
Pin::Logic GetMOSI()
{	 
	return MOSI;
}	 
Pin::Logic GetCS()
{	 
	return CS;
}	 
Pin::Logic GetCLK()
{
	return CLK;
}

void Nothing(Pin::Direction pValue)
{
}
Pin::Direction Nothing()
{
	return Pin::Direction::Input;
}

int main()
{
	using pin = Pin::logic_pin;
	using dir = Pin::direction_pin;

	//Pin definitions
	auto MISO_GS	= Pin(pin(GetMISO,	SetMISO),	dir(Nothing, Nothing));
	auto MOSI_GS	= Pin(pin(GetMOSI,	SetMOSI),	dir(Nothing, Nothing));
	auto CS_GS		= Pin(pin(GetCS,	SetCS),		dir(Nothing, Nothing));
	auto CLK_GS		= Pin(pin(GetCLK,	SetCLK),	dir(Nothing, Nothing));

	//
	Serial::byte const output = 12;
	{
		std::cout << "MODE 0" << endl;
		SPI Port0(SPI::Mode::Mode0, CLK_GS, CS_GS, MISO_GS, MOSI_GS);
		SPI_ResetRecord();
		Port0.Write(output);
		SPI_Print();

		std::cout << endl << "MODE 1" << endl;
		SPI Port1(SPI::Mode::Mode1, CLK_GS, CS_GS, MISO_GS, MOSI_GS);
		SPI_ResetRecord();
		Port1.Write(output);
		SPI_Print();

		std::cout << endl << "MODE 2" << endl;
		SPI Port2(SPI::Mode::Mode2, CLK_GS, CS_GS, MISO_GS, MOSI_GS);
		SPI_ResetRecord();
		Port2.Write(output);
		SPI_Print();

		std::cout << endl << "MODE 3" << endl;
		SPI Port3(SPI::Mode::Mode3, CLK_GS, CS_GS, MISO_GS, MOSI_GS);
		SPI_ResetRecord();
		Port3.Write(output);
		SPI_Print();
	}

	//
	//{
	//	std::cout << endl << "I2C 7 BIT" << endl;
	//	I2C I2C_Port0(I2C::Bits7, CLK_GS, MOSI_GS);
	//	I2C_Port0.SetAddress(0x10);
	//	SPI_ResetRecord();
	//	I2C_Port0.Write(0x14);
	//	SPI_Print();
	//}

	system("PAUSE");
    return 0;
}

