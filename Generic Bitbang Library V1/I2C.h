#ifndef I2C_H
#define I2C_H
#include "Serial.h"

class I2C : public Serial
{
public:
	using address = unsigned short;
	enum Type
	{
		Bits7	= false,
		Bits10	= true
	};

	I2C(Type const pType, Pin const pCLK, Pin const pDAT) : 
			 mType(pType),	   mCLK(pCLK),     mDAT(pDAT)
	{
		//Need to change when debug mode is off, needs to be OpenDrain
		mCLK.SetType(Drive::PushPull);
		mDAT.SetType(Drive::PushPull);

		mCLK = Logic::High;
		mDAT = Logic::High;
	}

	void SetAddress ( address pAddress )
	{
		//Mask the address
		switch (mType)
		{
		case Type::Bits7:
			//Mask 7 bits
			pAddress = ((pAddress & 0x7f) << 1) << 8;
			break;
		case Type::Bits10:
			//Mask bottom 8 bits
			byte lsb = (pAddress & 0xff);
			byte msb = (pAddress & 0x0300) << 1;
			pAddress = (msb << 8) | lsb;
			pAddress |= 0xF000;
			break;
		};

		//Shift the address to make sense
		mAddress = pAddress;
	}
	
	Serial::byte const Read ( )
	{
		StartCondition();
		SetMode( AddressType::Read );
		StartCondition();
		byte buffer = Clock8();
		EndCondition();
		return buffer;
	}
	void Write ( Serial::byte pInput )
	{
		StartCondition();
		SetMode ( AddressType::Write );
		StartCondition();
		Send(pInput);
		EndCondition();
	}
private:
	Type	mType;
	address mAddress;
	Pin		mCLK, mDAT;
	enum class AddressType
	{
		Read = true,
		Write = false
	};

	inline void ReleaseBus()
	{
		mCLK = Logic::High;
		mDAT = Logic::High;
	}
	inline void StartCondition()
	{
		mDAT = Logic::Low;
		mCLK = Logic::Low;
	}
	inline void ReleaseData()
	{
		mDAT = Logic::High;
	}
	inline void EndCondition()
	{
		mDAT = Logic::Low;
		ReleaseBus();
	}

	void ClockHigh()
	{
		mCLK = Logic::High;

		//This allows clock stretching
		auto clk = (Logic)mCLK;
		while (clk != Logic::High)
			clk = (Logic)mCLK;
	}
	inline void Clock()
	{
		ClockHigh();
		mCLK = Logic::Low;
	}
	inline Logic ACK()
	{
		mDAT = Logic::High;
		Clock();
		return mDAT;
	}
	inline void Send (byte pInput)
	{
		//Start clock low
		for ( auto i = 0; i < 8; ++i )
		{
			mDAT = (Logic)(pInput >= 0x80);
			Clock();
			pInput <<= 1;
		}
		ACK();
	}
	inline byte Clock8()
	{
		byte buffer = 0x00;
		for (auto i = 0; i < 8; ++i)
		{
			Clock();
			buffer = (buffer << 1) | mDAT;
		}
		ACK();
		return buffer;
	}
	
	inline address const SetAddress(AddressType pType) const
	{
		bool type = static_cast<bool>(pType);
		return (mAddress | (( type ) ? (1 << 8) : 0));
	}
	inline void SetMode(AddressType pType)
	{
		auto address = SetAddress(pType);

		//Always send first byte
		Send(address >> 8);

		//If 10bit mode
		if (mType)
			Send(address & 0xFF);

		ReleaseData();
	}
};
#endif