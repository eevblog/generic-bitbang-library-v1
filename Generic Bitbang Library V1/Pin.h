#ifndef PIN_H
#define PIN_H
#include "GetSet.h"

class Pin
{
public:
	//Types define IO behaviour
	enum class	Drive
	{
		Input,
		PushPull,
		OpenDrain,
		OpenSource
	};
	enum		Direction
	{
		Output = 0,
		Input = 1
	};
	enum		Logic
	{
		Low = 0,
		High = 1
	};

	//General consistant template definitions
	using		logic_pin		= GetSet<Logic>;
	using		direction_pin	= GetSet<Direction>;
	
	//Constructors
	Pin				(logic_pin pPin, direction_pin pDirection);
	Pin(Drive pType, logic_pin pPin, direction_pin pDirection);

	//Allows changing of pin type
	void SetType(Drive pType);

	//Operators that allow easy interface
	void operator=(Logic pInput);
	Logic operator()();
	operator Logic();

private:
	Drive				mType;
	direction_pin		mDirection;
	logic_pin			mPin;
};

#endif